//
//  ViewController.swift
//  TrabalhoExtra
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato{
    let nome: String
    let numero: String
    let email: String
    let endereco: String
    
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.telefone.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome:"Contato 1", numero: "31 98989-0000", email: "contato1@email.com.br", endereco: "Rua do bairro, 01" ))
        listaDeContatos.append(Contato(nome:"Contato 2", numero: "31 99898-0000", email: "contato2@email.com.br", endereco: "Rua do bairro, 02 " ))
        listaDeContatos.append(Contato(nome:"Contato 3", numero: "31 98787-0000", email: "contato3@email.com", endereco: "Rua do bairro, 03" ))
        listaDeContatos.append(Contato(nome:"Contato 4", numero: "31 99696-0000", email: "contato4@gmail.com", endereco: "Rua do bairro, 04" ))
        
    }


}

